## Information

Test do it by Adrián García Robles

## Versions used

Angular CLI: 7.1.4
Node: 10.14.2
OS: darwin x64
Angular: 7.1.4

## Hot to run it.

First we need to install the package by doing 'npm install', then we need to run 'ng serve' to see the result on the 'localhost:4200'
