import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// Custom Components
import { LogoComponent } from './logo/logo.component';
import { ToDoItemComponent } from './to-do-item/to-do-item.component';
import { ToDoComponent } from './to-do/to-do.component';
import { ByTypePipe } from '../pipes/by-type.pipe';

// Shared module components
@NgModule({
	imports: [
		CommonModule,
		FormsModule
	],
	declarations: [
		LogoComponent,
		ToDoItemComponent,
		ToDoComponent,
		ByTypePipe
	],
	exports: [
		LogoComponent,
		ToDoItemComponent,
		ToDoComponent,
		ByTypePipe
	]
})
export class ComponentsSharedModule {}
