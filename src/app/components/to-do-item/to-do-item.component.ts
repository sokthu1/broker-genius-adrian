import { Component, Input } from '@angular/core';

// Interfaces
import { ToDoItemInterface } from 'src/app/interfaces/to-do.interface';

@Component({
	selector: 'app-to-do-item',
	templateUrl: './to-do-item.component.html'
})
export class ToDoItemComponent {
	@Input() articles: ToDoItemInterface[] = [];

	@Input() type: string;

	constructor() {}

	/* 	onToggle function: To make done/undone the task
	* 	id {number} The id of the item we want to change the state to done/undone
	*		currentValue {boolean} The current value to change it. From true to false and also the other side
	*/
	onToggle(id: number, currentValue: boolean): void {
		if (this.type !== 'done') {
			return;
		}
		this.updateArray(id, 'done', !currentValue);
	}

	/* 	onArchive function: Change the status of the item to archive
	* 	id {number} The id of the item we want to archive
	*/
	onArchive(id: number): void {
		this.updateArray(id, 'archived', true);
	}

	/* 	onRemove function: To delete an item from the list
	* 	id {number} The id of the item we want to delete
	*/
	onRemove(id: number): void {
		if (!this.articles.find(item => item.id === id)) {
			return;
		}
		this.articles.splice(this.articles.findIndex(item => item.id === id), 1);
	}

	/* 	onUnarchive function: Change the status of the item to unarchive
	* 	id {number} The id of the item we want to unarchive
	*/
	onUnarchive(index: number): void {
		this.updateArray(index, 'archived', false);
	}

	/* 	Function to track by the id of the item in the list.
	*		index {number} The index of the item in the list.
	*		item {ToDoItemInterface} The entire item of the list.
	*/
	trackByFn(index, item) {
		return item.id;
	}

	/* 	To update the parent array that contains the list of doto tasks.
	*		id {id} The id of the item we want to update
	*		field {string} The name of the field we want to update
	*		value {boolean} The value that we want to uptate
	*/
	private updateArray(id: number, field: string, value: boolean): void {
		this.articles.forEach((toDoItem) => {
			if (toDoItem.id === id) {
				toDoItem[field] = value;
				return;
			}
		});
	}
}
