import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToDoItemComponent } from './to-do-item.component';

import { ByTypePipe } from 'src/app/pipes/by-type.pipe';

describe('ToDoItemComponent', () => {
	let component: ToDoItemComponent;
	let fixture: ComponentFixture<ToDoItemComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ToDoItemComponent, ByTypePipe]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ToDoItemComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
	it('should toggle to the opposite, done to undone', () => {
		const example = [{
			id: 1,
			text: 'test',
			done: true,
			archived: true
		}];
		component.type = 'done';
		component.articles = example;
		component.onToggle(example[0].id, example[0].done);
		expect(example[0].done).toBeFalsy();
	});
	it('should return the same done value, because you can\'t toggle on archived type', () => {
		const example = [{
			id: 1,
			text: 'test',
			done: true,
			archived: true
		}];
		component.type = 'archived';
		component.articles = example;
		component.onToggle(example[0].id, example[0].done);
		expect(example[0].done).toBeTruthy();
	});
	it('should archive the item of the list', () => {
		const example = [{
			id: 1,
			text: 'test',
			done: true,
			archived: false
		}];
		component.type = 'done';
		component.articles = example;
		component.onArchive(example[0].id);
		expect(example[0].archived).toBeTruthy();
	});
	it('should remove the item of the list', () => {
		const example = [{
			id: 1,
			text: 'test',
			done: true,
			archived: false
		}];
		component.type = 'done';
		component.articles = example;
		component.onRemove(example[0].id);
		expect(example).toEqual([]);
	});
	it('should remove the item of the list but we can\'t find it, so return the same', () => {
		const example = [{
			id: 1,
			text: 'test',
			done: true,
			archived: false
		}];
		component.type = 'done';
		component.articles = example;
		component.onRemove(100);
		expect(example).toEqual(example);
	});
	it('should unarchive the object', () => {
		const example = [{
			id: 1,
			text: 'test',
			done: true,
			archived: true
		}];
		component.type = 'archived';
		component.articles = example;
		component.onUnarchive(example[0].id);
		expect(example[0].archived).toBeFalsy();
	});
});
