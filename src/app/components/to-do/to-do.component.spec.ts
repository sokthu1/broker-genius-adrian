import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import * as uuid from 'uuid';


import { ByTypePipe } from 'src/app/pipes/by-type.pipe';

import { TO_DO_CONSTANTS } from 'src/app/constants/to-do.constants';

import { ToDoComponent } from './to-do.component';
import { ToDoItemComponent } from '../to-do-item/to-do-item.component';

describe('ToDoComponent', () => {
	let component: ToDoComponent;
	let fixture: ComponentFixture<ToDoComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				ToDoComponent,
				ToDoItemComponent,
				ByTypePipe
			],
			imports: [FormsModule]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ToDoComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('Expect showErrorFormMessage to be Falsy becasue taskName is empty', () => {
		component.taskName = '';
		component.onCreateSubmitBtn();
		expect(component.showErrorFormMessage).toBeTruthy();
	});

	it('Expect toDoList to have a new item, created by onCreateSubmitBtn()', () => {
		component.taskName = 'Test item';
		component.onCreateSubmitBtn();
		const spy = spyOn(uuid, 'v4').and.callFake(() => {
			return 100;
		});
		const shouldReturnExample = TO_DO_CONSTANTS;
		shouldReturnExample.push({
			id: 100,
			text: 'Test item',
			done: false,
			archived: false
		});

		expect(component.toDoList).toEqual(shouldReturnExample);
		expect(component.taskName).toEqual('');
	});
});
