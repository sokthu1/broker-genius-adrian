import { Component } from '@angular/core';

import * as uuid from 'uuid';

import { ToDoItemInterface } from 'src/app/interfaces/to-do.interface';

import { TO_DO_CONSTANTS } from 'src/app/constants/to-do.constants';

@Component({
	selector: 'app-to-do',
	templateUrl: './to-do.component.html'
})
export class ToDoComponent {
	// Our initial value of the to do list.
	toDoList: ToDoItemInterface[] = TO_DO_CONSTANTS;
	// Model of the add form
	taskName: string = '';
	// Control for the add button. Submit in the form
	showErrorFormMessage: boolean = false;

	constructor() {}

	onCreateSubmitBtn(): void {
		const hasTaskNameValue = this.taskName !== '';

		if (hasTaskNameValue) {
			this.toDoList.push(this.buildItem());
			this.taskName = '';
			this.showErrorFormMessage = false;
			return;
		}

		this.showErrorFormMessage = true;
	}
	/*	Function to build an object to add in the current list
	* 	return {ToDoItemInterface} The object to add in the do to list
	*/
	private buildItem(): ToDoItemInterface {
		return {
			id: uuid.v4(),
			text: this.taskName,
			done: false,
			archived: false
		};
	}

}
