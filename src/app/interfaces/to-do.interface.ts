export interface ToDoItemInterface {
	id: number;
	text: string;
	done: boolean;
	archived: boolean;
}
