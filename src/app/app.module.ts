import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

// Main components
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// Pages
import { AppPagesModule } from './pages/app-pages.module';

// Components
import { ComponentsSharedModule } from './components/components-shared.module';

// Pipes
import { ByTypePipe } from './pipes/by-type.pipe';

@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		AppPagesModule,
		ComponentsSharedModule
	],
	exports: [
	],
	providers: [],
	bootstrap: [AppComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
