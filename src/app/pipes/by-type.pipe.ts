import { Pipe, PipeTransform } from '@angular/core';

import { ToDoItemInterface } from '../interfaces/to-do.interface';

@Pipe({
	name: 'byType',
	pure: false
})
export class ByTypePipe implements PipeTransform {

	transform(items: ToDoItemInterface[], field: string): ToDoItemInterface[] {
		if (items.length === 0) {
			return [];
		}

		if (!field) {
			return items;
		}

		switch (field) {
			case 'done':
				return items.filter(singleItem =>
					!singleItem['archived']
				);
			case 'archived':
				return items.filter(singleItem =>
					singleItem['archived']
				);
			default:
				return items;
		}

	}
}
