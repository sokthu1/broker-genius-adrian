import { ByTypePipe } from './by-type.pipe';

import { ToDoItemInterface } from '../interfaces/to-do.interface';

describe('ByTypePipe', () => {
	let DoToListExample: ToDoItemInterface[];
	it('create an instance', () => {
		const pipe = new ByTypePipe();
		expect(pipe).toBeTruthy();
	});
	it('Should return an empty array', () => {
		const pipe = new ByTypePipe();
		const result = pipe.transform([], 'field');
		expect(result).toEqual([]);
	});

	beforeEach(() => {
		DoToListExample = [
			{
				id: 1,
				text: 'text 1',
				done: false,
				archived: false
			}, {
				id: 2,
				text: 'text 2',
				done: false,
				archived: false
			}, {
				id: 3,
				text: 'text 3',
				done: true,
				archived: false
			}, {
				id: 4,
				text: 'text 4',
				done: false,
				archived: true
			}, {
				id: 5,
				text: 'text 5',
				done: true,
				archived: true
			}, {
				id: 6,
				text: 'text 6',
				done: false,
				archived: false
			}
		];
	});

	it('Should return the full to_do_constant', () => {
		const shouldBeResult = DoToListExample;
		const pipe = new ByTypePipe();
		const result = pipe.transform(DoToListExample, null);
		expect(DoToListExample).toEqual(shouldBeResult);
	});
	it('Should return the full to_do_constant, by default switch', () => {
		const shouldBeResult = DoToListExample;
		const pipe = new ByTypePipe();
		const result = pipe.transform(DoToListExample, 'test');
		expect(result).toEqual(shouldBeResult);
	});
	it('Should return the to_do_constant but just the archieved false list', () => {
		const shouldBeResult = [{
			id: 1,
			text: 'text 1',
			done: false,
			archived: false
		}, {
			id: 2,
			text: 'text 2',
			done: false,
			archived: false
		}, {
			id: 3,
			text: 'text 3',
			done: true,
			archived: false
		}, {
			id: 6,
			text: 'text 6',
			done: false,
			archived: false
		}];
		const pipe = new ByTypePipe();
		const result = pipe.transform(DoToListExample, 'done');
		expect(result).toEqual(shouldBeResult);
	});
	it('Should return the full to_do_constant, just the achived true list', () => {
		const shouldBeResult = [{
			id: 4,
			text: 'text 4',
			done: false,
			archived: true
		}, {
			id: 5,
			text: 'text 5',
			done: true,
			archived: true
		}];
		const pipe = new ByTypePipe();
		const result = pipe.transform(DoToListExample, 'archived');
		expect(result).toEqual(shouldBeResult);
	});
});
