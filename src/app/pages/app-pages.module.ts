import { NgModule } from '@angular/core';

// Custom components
import { ComponentsSharedModule } from '../components/components-shared.module';

// Pages
import { NotFoundComponent } from './not-found/not-found.component';
import { HomeComponent } from './home/home.component';


@NgModule({
	declarations: [
		HomeComponent,
		NotFoundComponent
	],
	imports: [ComponentsSharedModule]
})
export class AppPagesModule {}
