import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { ByTypePipe } from 'src/app/pipes/by-type.pipe';

import { HomeComponent } from './home.component';
import { ToDoComponent } from 'src/app/components/to-do/to-do.component';
import { ToDoItemComponent } from 'src/app/components/to-do-item/to-do-item.component';

describe('HomeComponent', () => {
	let component: HomeComponent;
	let fixture: ComponentFixture<HomeComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				HomeComponent,
				ToDoComponent,
				ToDoItemComponent,
				ByTypePipe
			],
			imports: [FormsModule]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(HomeComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
